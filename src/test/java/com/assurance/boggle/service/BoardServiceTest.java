package com.assurance.boggle.service;

import com.assurance.boggle.controller.BoardController;
import com.assurance.boggle.entity.BoardEntity;
import com.assurance.boggle.entity.SubmittedWordEntity;
import com.assurance.boggle.repo.BoardRepository;
import com.assurance.boggle.repo.SubmittedWordRepository;
import com.assurance.boggle.util.Boggle;
import com.assurance.boggle.util.Dictionary;
import javafx.beans.binding.When;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Set;

import static com.assurance.boggle.util.SettingUtil.MAX_WORD_LENGTH;
import static com.assurance.boggle.util.SettingUtil.MIN_PREFIX_LENGTH;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class BoardServiceTest extends AbstractServiceTest{

    @Mock
    private
    BoardRepository boardRepositoryMock;

    @Mock
    private SubmittedWordRepository submittedWordRepositoryMock;

    @InjectMocks
    private
    BoardService boardServiceMock;

    private Dictionary dictionary;

    @Before
    public void setUp(){
        URL url = BoardServiceTest.class.getClassLoader().getResource("boggle.dict.txt");
        try {
            dictionary = new Dictionary(new File(url.getFile()), MAX_WORD_LENGTH, MIN_PREFIX_LENGTH);
        } catch (IOException e){
            System.out.printf("io exception");
        }

    }

    @Test
    public void testSaveNewBoard() {
        BoardEntity boardEntity = new BoardEntity();
        Boggle boggle = new Boggle();
        Set<String> boardAllWords = boggle.findWords(dictionary);
        boardEntity.setBoard(Arrays.deepToString(boggle.board));
        boardEntity.setWords(boardAllWords);
        boardEntity.setTotalScore(0);
        boardServiceMock.saveNewBoard(boardEntity);
        verify(boardRepositoryMock, times(1)).save(boardEntity);
        verifyNoMoreInteractions(boardRepositoryMock);
    }

    @Test
    public void testSubmitWord() {
        Long boardId = 1L;
        String word = "sonse";
        boardServiceMock.submitWord(boardId, word);
        verify(boardRepositoryMock, times(1)).getOne(boardId);
        verifyNoMoreInteractions(boardRepositoryMock);

        SubmittedWordEntity submittedWordEntity = new SubmittedWordEntity();

        verify(submittedWordRepositoryMock, times(1)).save(submittedWordEntity);
        verifyNoMoreInteractions(submittedWordRepositoryMock);
    }

    @Test
    public void testSubmitGame() {
        Long boardId = 1L;
        Integer totalScore = 15;
        boardServiceMock.submitGame(boardId, totalScore);
        BoardEntity boardEntity = new BoardEntity();
        verify(boardRepositoryMock, times(1)).getOne(boardId);
        verify(boardRepositoryMock, times(1)).save(boardEntity);
        verifyNoMoreInteractions(boardRepositoryMock);
    }

}
