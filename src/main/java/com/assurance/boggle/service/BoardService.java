package com.assurance.boggle.service;

import com.assurance.boggle.entity.BoardEntity;
import com.assurance.boggle.entity.SubmittedWordEntity;
import com.assurance.boggle.exception.BoggleException;
import com.assurance.boggle.repo.BoardRepository;
import com.assurance.boggle.repo.SubmittedWordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BoardService {

    private final BoardRepository boardRepository;
    private final SubmittedWordRepository submittedWordRepository;

    @Autowired
    BoardService(BoardRepository boardRepository,
                 SubmittedWordRepository submittedWordRepository){
        this.boardRepository=boardRepository;
        this.submittedWordRepository=submittedWordRepository;
    }

    public BoardEntity saveNewBoard(BoardEntity board){
       return boardRepository.save(board);
    }

    public void submitWord(Long boardId, String word) {
        BoardEntity board = boardRepository.getOne(boardId);
        Set<String> submittedWords = board.getSubmittedWords().stream().map(SubmittedWordEntity::getWord).collect(Collectors.toSet());
        Set<String> boardAllWords = board.getWords();

        if (submittedWords.contains(word)){
            throw new BoggleException("W001");
        }
        if(!boardAllWords.contains(word)){
            throw new BoggleException("W002");
        }

        SubmittedWordEntity submittedWordEntity = new SubmittedWordEntity();
        submittedWordEntity.setWord(word);
        submittedWordEntity.setBoard(board);
        submittedWordRepository.save(submittedWordEntity);
    }

    public Map<String,Object> submitGame(Long boardId, Integer totalScore) {
        BoardEntity board = boardRepository.getOne(boardId);
        board.setTotalScore(totalScore);
        boardRepository.save(board);

        Set<String> submittedWords = board.getSubmittedWords().stream().map(SubmittedWordEntity::getWord).collect(Collectors.toSet());
        Set<String> boardAllWords = board.getWords();

        Map<String, Object> response = new HashMap<>();
        response.put("submittedWords", submittedWords);
        response.put("boardAllWords", boardAllWords);
        response.put("totalScore", totalScore);
        return response;
    }
}
