package com.assurance.boggle.util;

public class SettingUtil {

    public static Dictionary dictionary = null;
    public static int DEFAULT_SIZE = 4; // Default dimensions of grid
    public static int MIN_PREFIX_LENGTH = 2; // Smallest number of characters in a prefix tree
    public static int MIN_WORD_LENGTH = 2; // Lowest number of letters in a word.
    public static int MAX_WORD_LENGTH = 32; // Highest number of letters in a  match.
}
