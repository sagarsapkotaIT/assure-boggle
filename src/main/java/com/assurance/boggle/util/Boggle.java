package com.assurance.boggle.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import static com.assurance.boggle.util.SettingUtil.*;

public class Boggle {

    private int size; // Dimensions of grid
    private Cell[][] grid; // Boggle grid
    public char[][] board; // multidimensional character array board

    private Random generator = new Random();

    /**
     * Class Cell represents a character Cell in the grid.
     */
    private class Cell {
        boolean visited;
        char c;

        Cell(char c) {
            this.c = c;
            visited = false;
        }

        public String toString() {
            return Character.toString(c);
        }
    }

    /**
     * Creates a new boggle board of the default dimensions, filled with random
     * characters.
     *
     */
    public Boggle() {
        generator = new Random();
        size = DEFAULT_SIZE;
        grid = new Cell[size][size];
        board = new char[size][size];
        char rChar;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                rChar = randomChar();
                grid[i][j] = new Cell(rChar);
                board[i][j] = rChar;
            }
        }
    }

    /**
     * Searches the board for matches in the given Dictionary. Prints matches as
     * they are found.
     *
     * @param dictionary The dictionary to check against.
     * @return A tree of found words.
     */
    public Set<String> findWords(Dictionary dictionary) {
        Set<String> matches = new HashSet<>();
        StringBuffer charBuffer = new StringBuffer();

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                findWords(dictionary, matches, i, j, charBuffer);
            }
        }

        return matches;
    }


    /**
     * Recursively searches a Cell for words
     *
     * @param dictionary Dictionary of valid words
     * @param matches Words found in the board
     * @param i Vertical index of word in board
     * @param j Horizontal index of word in board
     * @param charBuffer Characters this method has traversed
     */
    private void findWords(Dictionary dictionary, Set<String> matches,
                           int i, int j, StringBuffer charBuffer) {

        // Whether or not this is a valid beginning to a word
        boolean valid_word_prefix = true;

        // Add this cell to character buffer
        charBuffer.append(grid[i][j].c);

        // Mark cell as visited
        grid[i][j].visited = true;

        // Set word
        String word = new String(charBuffer);

        // Word candidate
        if (charBuffer.length() >= MIN_WORD_LENGTH
                && charBuffer.length() <= MAX_WORD_LENGTH) {
            // The number of characters we have is within the acceptable range
            // for a word.

            // Check word for word-i-ness.
            if (dictionary.getIndex().find(word)) {
                // This is a word
                if (matches.add(word)) {
                    // We haven't seen this word before
                    System.out.println(matches.size() + ": " + word);
                }
            }
        }

        /*
         * Check word for word-ability Essentially, we look to see if this word
         * *might* be a word later by checking if it exists in a prefix tree,
         * built from progressively larger initial substrings of words.
         */
        if (charBuffer.length() >= MIN_PREFIX_LENGTH
                && charBuffer.length() < MAX_WORD_LENGTH) {
            // System.out.println("Testing word " + word + " of length "
            // + charBuffer.length() + " for prefix.");
            // We can test this word to see if it's a prefix
            if (!dictionary.getPrefix()[charBuffer.length()].find(word)) {
                // This is not a valid word prefix
                // System.out.println("Not a potential word");
                valid_word_prefix = false;
            }
        }

        if (valid_word_prefix && charBuffer.length() < MAX_WORD_LENGTH) {
            // This is a valid word prefix (will be a word later), and adding
            // another letter will not exceed the target maximum word length.
            // Check recursively.
            for (int iTarget = i - 1; iTarget <= i + 1; iTarget++) {
                if (iTarget < 0 || iTarget >= size) {
                    // index iTarget is out of bounds
                    continue;
                }
                for (int jTarget = j - 1; jTarget <= j + 1; jTarget++) {
                    if (jTarget < 0 || jTarget >= size) {
                        // index jTarget is out of bounds
                        continue;
                    }
                    if (!grid[iTarget][jTarget].visited) {
                        // Target Cell has not been visited
                        findWords(dictionary, matches, iTarget, jTarget, charBuffer);
                    }
                }
            }
        }
        // Since we're returning control, this Cell is no longer visited
        grid[i][j].visited = false;
        // Remove Cell from character stack
        charBuffer.deleteCharAt(charBuffer.length() - 1);
    }

    /**
     * Returns a human-readable representation of the board.
     */
    public String toString() {
        String str = "|";
        for (int j = 0; j < size * 4 - 1; j++) {
            str = str.concat("-");
        }
        str = str + "|\n";

        for (int i = 0; i < size; i++) {
            str = str.concat("| ");
            for (int j = 0; j < size; j++) {
                str = str.concat((grid[i][j]).toString());
                str = str + " | ";
            }
            str = str + "\n|";
            for (int j = 0; j < size * 4 - 1; j++) {
                str = str .concat("-");
            }
            str = str + "|\n";
        }
        return str;
    }

    /**
     * Returns a random lowercase character from a to z roughly matching the
     * frequency distribution of english text.
     */
    private char randomChar() {
        float i = generator.nextInt(1000000);
        i = i / 1000000;
        if (i < .08167) {
            return 'a';
        }
        if (i < .09659) {
            return 'b';
        }
        if (i < .12441) {
            return 'c';
        }
        if (i < .16694) {
            return 'd';
        }
        if (i < .29396) {
            return 'e';
        }
        if (i < .31624) {
            return 'f';
        }
        if (i < .33639) {
            return 'g';
        }
        if (i < .39733) {
            return 'h';
        }
        if (i < .46699) {
            return 'i';
        }
        if (i < .46852) {
            return 'j';
        }
        if (i < .47624) {
            return 'k';
        }
        if (i < .51649) {
            return 'l';
        }
        if (i < .54055) {
            return 'm';
        }
        if (i < .60804) {
            return 'n';
        }
        if (i < .68311) {
            return 'o';
        }
        if (i < .70240) {
            return 'p';
        }
        if (i < .70335) {
            return 'q';
        }
        if (i < .76322) {
            return 'r';
        }
        if (i < .82649) {
            return 's';
        }
        if (i < .91705) {
            return 't';
        }
        if (i < .94463) {
            return 'u';
        }
        if (i < .95441) {
            return 'v';
        }
        if (i < .97801) {
            return 'w';
        }
        if (i < .97951) {
            return 'x';
        }
        if (i < .99925) {
            return 'y';
        }
        if (i < 1) {
            return 'z';
        } else {
            // Failsafe
            return 'e';
        }
    }
}
