package com.assurance.boggle.util;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Dictionary {

    private BinarySearchTree index;
    /**
     * Prefix is an array of binary search trees where the tree located at
     * element i in the array contains all substrings of length i at index 0
     * in the dictionary file. Hence, the word 'catch' is stored in [1] as
     * 'c', [2] as 'ca', [3] as 'cat', etc. This allows the search function
     * to abort search paths which can never result in words.
     */
    private BinarySearchTree[] prefix;

    /**
     * Constructs a dictionary
     *
     * @param file File to parse
     * @throws IOException If file can't be read.
     */
    public Dictionary(File file, Integer maxWordLength, Integer maxPrefixLength) throws IOException {
        index = new BinarySearchTree();
        prefix = new BinarySearchTree[maxWordLength];

        // Initialize prefix BSTs
        for (int i = 0; i < prefix.length; i++) {
            prefix[i] = new BinarySearchTree();
        }

        String word;

        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("[\\n]"); // Separate tokens using newlines

        // Add words to indexes
        while (scanner.hasNext()) {
            // Get word
            word = scanner.next();

            if (word.length() > maxWordLength) {
                // Word is too long, skip
                continue;
            }

            // Lowercase
            word = word.toLowerCase();

            // Add word to primary dictionary
            index.add(word);

            // Add word prefixes to prefix dictionaries
            for (int i = maxPrefixLength; i < prefix.length && i < word.length(); i++) {
                prefix[i].add(word.substring(0, i));
            }
        }
    }

    BinarySearchTree getIndex() {
        return index;
    }

    BinarySearchTree[] getPrefix() {
        return prefix;
    }

}
