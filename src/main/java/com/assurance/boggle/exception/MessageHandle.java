package com.assurance.boggle.exception;

import com.assurance.boggle.controller.BoardController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class MessageHandle {

    private static final Logger LOG = LoggerFactory.getLogger(MessageHandle.class);

    static String getMessage(String key) {
        return getPropertyValue(key);
    }

    private static String getPropertyValue(String key) {
        try {
            Properties properties = readFileAsProperties();
            return properties.getProperty(key);
        } catch (IOException ioe) {
            LOG.error("----- Error occurred while reading the file. -----");
            return null;
        }
    }

    private static Properties readFileAsProperties() throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("error-code.properties");
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;
    }

}
