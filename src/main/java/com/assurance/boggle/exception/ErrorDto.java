package com.assurance.boggle.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class ErrorDto {


    private int status;
    private String errorCode;
    private String errorMessage;

    ErrorDto(int status, String errorCode) {
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessage = MessageHandle.getMessage(this.errorCode);
    }
}
