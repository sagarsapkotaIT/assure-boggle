package com.assurance.boggle.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoggleException extends RuntimeException{

    private String errorCode;
    private String errorMessage;

    public BoggleException(String errorCode) {
        this.errorCode = errorCode;
        this.errorMessage = MessageHandle.getMessage(this.errorCode);
    }

}
