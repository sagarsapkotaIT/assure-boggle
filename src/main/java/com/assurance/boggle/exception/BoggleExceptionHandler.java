package com.assurance.boggle.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.nio.file.AccessDeniedException;

@RestControllerAdvice
public class BoggleExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @ExceptionHandler(value = BoggleException.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    public ErrorDto handleAccountingException(BoggleException accountingException) {
        logger.error("----- " + accountingException.getErrorMessage() + " -----");
        accountingException.printStackTrace();
        return new ErrorDto(HttpStatus.EXPECTATION_FAILED.value(), accountingException.getErrorCode());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseStatus(HttpStatus.EXPECTATION_FAILED)
    public ErrorDto handleException(Exception e) {
        logger.error("----- " + e.getMessage() + " -----");
        e.printStackTrace();
        return new ErrorDto(HttpStatus.EXPECTATION_FAILED.value(), "ERR001");
    }

}
