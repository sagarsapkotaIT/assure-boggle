package com.assurance.boggle.dto;

import java.util.HashMap;
import java.util.Map;
/*
* this class is used to build the response of the service
* */
public class ServiceResponse {
    private boolean success;
    private String message;
    private Map<String, Object> params = new HashMap<>();

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public ServiceResponse setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public ServiceResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    public ServiceResponse addParams(String key, Object value) {
        this.params.put(key, value);
        return this;
    }
}
