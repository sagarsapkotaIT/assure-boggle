package com.assurance.boggle.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "submitted_words")
@Setter(AccessLevel.PUBLIC)
@Getter(AccessLevel.PUBLIC)
@EntityListeners(AuditingEntityListener.class)
public class SubmittedWordEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;


    @Column(name = "word")
    private String word;


    @ManyToOne
    @JoinColumn(name = "boardId")
    private BoardEntity board;

    @CreatedDate
    @Column(name = "created_date")
    private Date createdDate;

    @LastModifiedDate
    @Column(name = "last_modified")
    private Date last_modified;
}
