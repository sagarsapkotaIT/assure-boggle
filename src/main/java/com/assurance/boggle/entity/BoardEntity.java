package com.assurance.boggle.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "boards")
@Getter(AccessLevel.PUBLIC)
@Setter(AccessLevel.PUBLIC)
@EntityListeners(AuditingEntityListener.class)
public class BoardEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "board")
    private String board;

    @Column(name = "total_score")
    private Integer totalScore;

    @ElementCollection
    @CollectionTable(name = "all_words", joinColumns = @JoinColumn(name = "board_id"))
    @Column(name = "all_words")
    private Set<String> words;


    @CreatedDate
    @Column(name = "created_date")
    private Date createdDate;

    @LastModifiedDate
    @Column(name = "last_modified")
    private Date last_modified;


    @OneToMany(mappedBy = "board", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<SubmittedWordEntity> submittedWords;


}
