package com.assurance.boggle.repo;

import com.assurance.boggle.entity.SubmittedWordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubmittedWordRepository extends JpaRepository<SubmittedWordEntity, Long> {
}
