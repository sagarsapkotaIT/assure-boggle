package com.assurance.boggle.controller;

import com.assurance.boggle.dto.ServiceResponse;
import com.assurance.boggle.entity.BoardEntity;
import com.assurance.boggle.service.BoardService;
import com.assurance.boggle.util.Boggle;
import com.assurance.boggle.util.Dictionary;
import java.net.URL;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static com.assurance.boggle.util.SettingUtil.MAX_WORD_LENGTH;
import static com.assurance.boggle.util.SettingUtil.MIN_PREFIX_LENGTH;
import static com.assurance.boggle.util.SettingUtil.dictionary;

@RestController
@RequestMapping(value = "/board")
public class BoardController {

    private static final Logger LOG = LoggerFactory.getLogger(BoardController.class);

    private final BoardService boardService;

    @Autowired
    BoardController(BoardService boardService){
        this.boardService=boardService;
    }

    @ApiOperation(value = "Get a newly generated board.")
    @RequestMapping(value = "/getNewBoard", method = RequestMethod.GET)
    public ResponseEntity<ServiceResponse> getNewBoard() {
        // Generate boggle board
        Boggle boggle = new Boggle();
        LOG.info("board is: {}", boggle);

        // Search board with dictionary
        long startSearchTime = System.currentTimeMillis();
        Set<String> boardAllWords = boggle.findWords(dictionary);
        long finishTime = System.currentTimeMillis();
        LOG.info("Searched board in " + ((finishTime - startSearchTime) / 1000.0) + " seconds.");

        //save the board
        BoardEntity board = new BoardEntity();
        board.setBoard(Arrays.deepToString(boggle.board));
        board.setWords(boardAllWords);
        board.setTotalScore(0);
        BoardEntity boardEntity = boardService.saveNewBoard(board);


        //return the response with new board
        return ResponseEntity.ok( new ServiceResponse()
                .setSuccess(true)
                .setMessage("board generated successfully")
                .addParams("board", boggle.board)
                .addParams("boardId", boardEntity.getId())
        );
    }


    @RequestMapping(value = "/submitWord/{boardId}/{word}", method = RequestMethod.GET)
    @ApiOperation(value = "Submit a word of the board.")
    public ResponseEntity<ServiceResponse> submitWord(@PathVariable Long boardId, @PathVariable String word) {

            //submit the word
            boardService.submitWord(boardId, word);

            //send response with score
            return ResponseEntity.ok( new ServiceResponse()
                    .setSuccess(true)
                    .setMessage("word has been submitted successfully")
                    .addParams("score", getScore(word))
            );

    }

    @RequestMapping(value = "/submitGame/{boardId}/{totalScore}", method = RequestMethod.GET)
    @ApiOperation(value = "Submit a word of the board.")
    public ResponseEntity<ServiceResponse> submitGame(@PathVariable Long boardId, @PathVariable Integer totalScore) {

        //submit the game
        Map<String, Object> response = boardService.submitGame(boardId, totalScore);

        //send response with score
        return ResponseEntity.ok( new ServiceResponse()
                .setSuccess(true)
                .setMessage("game has been submitted successfully")
                .addParams("response", response)
        );

    }

    private short getScore(String word){
        if(word.length()<=3){
            return 2;
        }else if(word.length()<=9){
            return 3;
        }else {
            return 5;
        }
    }

    @PostConstruct
    public void createDictionary() {
        try {
            long startSearchTime = System.currentTimeMillis();
            URL url = BoardController.class.getClassLoader().getResource("boggle.dict.txt");
            if(url==null){
                throw new Exception("dictionary file not found");
            }
            dictionary = new Dictionary(new File(url.getFile()), MAX_WORD_LENGTH, MIN_PREFIX_LENGTH);
            long finishTime = System.currentTimeMillis();
            LOG.info("dictionary generated in " + ((finishTime - startSearchTime) / 1000.0) + " seconds.");
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
