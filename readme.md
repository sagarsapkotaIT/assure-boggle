Boggle Solver
Backend section of web-based app for finding all possible dictionary words in a game of Boggle.

developed using spring boot framework and mysql

Binary search tree library and dictionary file are taken from the internet

this application is built for the purpose of submitting a test to Assurance